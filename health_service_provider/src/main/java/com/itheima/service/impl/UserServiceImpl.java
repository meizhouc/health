package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.MessageConstant;
import com.itheima.dao.MenuDao;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.entiy.Result;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;

    @Autowired
    private MenuDao menuDao;

    //根据用户名查询数据库获取用户信息和关联的角色信息 同时需要查询角色关联的权限信息
    @Override
    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);
        if (user == null){
            return null;
        }
        Integer userId = user.getId();
        //根据用户ID查询对应的角色
        Set<Role> roles = roleDao.findByUserId(userId);
        for (Role role : roles) {
            Integer roleId = role.getId();
            //根据角色ID查询关联的权限
            Set<Permission> permissions = permissionDao.findByRoleId(roleId);
            role.setPermissions(permissions);//让角色关联权限
        }
        user.setRoles(roles);//让用户关联角色
        return user;
    }


    //新增用户
    public void add(User user, Integer[] roleIds) {
        //新增用户操作t_user表
        userDao.add(user);
        //建立用户和角色的多对多关系
        Integer userId = user.getId();
        this.setUserAndRole(userId,roleIds);
    }

    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        //获取分页的所需条件
        Integer currentPage = queryPageBean.getCurrentPage();
        String queryString = queryPageBean.getQueryString();
        Integer pageSize = queryPageBean.getPageSize();

        //分页插件
        PageHelper.startPage(currentPage,pageSize);

        Page<User> page = userDao.findByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public User findById(Integer id) {
        return userDao.findById(id);
    }

    @Override
    public List<Integer> findRoleIdsByUserId(Integer id) {
        return userDao.findRoleIdsByUserId(id);
    }

    @Override
    public void edit(User user, Integer[] roleIds) {
        //修改用户基本信息,操作t_user表
        userDao.edit(user);
        //清理当前用户和角色权限的关联关系
        userDao.deleteAssocication(user.getId());
        //重建当前的关联关系
       Integer userId= user.getId();
       this.setUserAndRole(userId,roleIds);
    }

    @Override
    public void deleteById(Integer id) {
        userDao.deleteAssocication(id);
        userDao.deleteId(id);

    }

    //建立用户和角色的多对多关联关系
    public void setUserAndRole(Integer userId,Integer[] roleIds){
        if (userId!=null&&roleIds.length>0){
            for (Integer roleId : roleIds) {
                Map<String,Integer> map=new HashMap<>();
                map.put("use_Id",userId);
                map.put("role_Id",roleId);
                userDao.setUserAndRole(map);
            }
        }
    }

    //根据用户id查询用户可访问菜单
    @Override
    public Result menu(String username) {
        //1、查询用户是否有角色
        //通过用户名查询用户id
        User user = userDao.findByUsername(username);
        if(user == null){
            //用户不存在
            return new Result(false, MessageConstant.GET_MENU_FAIL);
        }

        //通过id查询角色
        Set<Role> roles = roleDao.findByUserId(user.getId());//通过id查询用户角色
        if(roles == null || roles.size() == 0){
            return new Result(false, MessageConstant.GET_MENU_FAIL);//用户没有权限
        }

        //2、查询角色可以访问的所有菜单
        //创建list集合用于存储完整返回菜单
        List<Map<String, Object>> menus = new ArrayList<>();

        //封装工作台子数据
        List<Map<String, Object>> children = new ArrayList<>();
        //封装工作台数据
        Map<String, Object> workbench = new HashMap<String, Object>();
        //封装地址管理数据
        Map<String, Object> map1 = new HashMap<String, Object>();

        map1.put("path","/1-1");
        map1.put("title","地址管理");
        map1.put("linkUrl","address_manager.html");
        children.add(map1);

        //加入工作台菜单及其子菜单
        workbench.put("path","1");
        workbench.put("title","工作台");
        workbench.put("icon","fa-dashboard");
        workbench.put("children",children);
        menus.add(workbench);

        for (Role role : roles) {
            //通过角色id获取可访问菜单
            List<Menu> list = menuDao.findMenuByid(role.getId());

            for (Menu menu : list) {
                //判断是否为1级菜单
                if(menu.getLevel() == 1){
                    //创建map集合用于封装每个1级菜单
                    Map<String, Object> map = new HashMap<String, Object>();

                    //创建list用于存储2级菜单
                    List<Map<String, Object>> list2 = new ArrayList<>();

                    //将可访问菜单的2级菜单添加到对应的1级菜单中
                    for (Menu menu2 : list) {

                        if(menu2.getLevel() == 2 && menu2.getParentMenuId().equals(menu.getId())){
                            //创建map用于封装页面需要的2级菜单数据
                            Map<String, Object> map2 = new HashMap<String, Object>();

                            //将符合的2级菜单存到list2中
                            map2.put("path",menu2.getPath());//封装路由路径
                            map2.put("title",menu2.getName());//封装菜单名称
                            map2.put("linkUrl",menu2.getLinkUrl());//封装访问路径
                            list2.add(map2);
                        }
                    }

                    //添加1级菜单到menus集合
                    map.put("path",menu.getPath());//封装路由路径
                    map.put("title",menu.getName());//封装菜单名称
                    map.put("icon",menu.getIcon());//封装图标
                    map.put("children",list2);//封装子菜单
                    menus.add(map);

                }
            }
        }

        return new Result(true,MessageConstant.GET_MENU_SUCCESS,menus);//3、返回成功数据
    }
}
