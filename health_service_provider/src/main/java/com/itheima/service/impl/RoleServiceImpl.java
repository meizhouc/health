package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.RoleDao;
import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {


    @Autowired
    private RoleDao roleDao;

    //新增角色 同时需要让角色关联权限
    @Override
    public void add(Role role, Integer[] permissionIds) {
        //新增角色  操作t_role表
        roleDao.add(role);
        //设置角色和权限的多对多关联关系
        Integer roleId = role.getId();
        this.setRoleAndPermission(roleId,permissionIds);
    }


    //建立角色和权限的多对多关联关系
    public void setRoleAndPermission(Integer roleId,Integer[] permissionIds){
        if (roleId != null && permissionIds.length >0){
            for (Integer permissionId : permissionIds) {
                Map<String,Integer> map = new HashMap<>();
                map.put("roleId",roleId);
                map.put("permissionId",permissionId);
                roleDao.setRoleAndPermission(map);
            }
        }
    }

    //分页查询
    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        //当前页码
        Integer currentPage = queryPageBean.getCurrentPage();
        //每页显示的记录数
        Integer pageSize = queryPageBean.getPageSize();
        //查询条件
        String queryString = queryPageBean.getQueryString();
        //完成分页查询 基于mybatis框架提供分页助手插件完成
        PageHelper.startPage(currentPage,pageSize);
        Page<Role> page = roleDao.findByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    //根据ID查询角色
    @Override
    public Role findById(Integer id) {
        return roleDao.findById(id);
    }

    //根据角色ID查询权限包含的多个角色ID
    @Override
    public List<Integer> findPermissionIdsByRoleId(Integer id) {
        return roleDao.findPermissionIdsByRoleId(id);
    }

    //编辑角色
    @Override
    public void edit(Role role, Integer[] permissionIds) {
        //修改角色基本信息 操作检查组t_role表
        roleDao.edit(role);
        //清理当前角色关联的权限 操作中间表
        roleDao.deleteAssocication(role.getId());
        //重新建立当前角色和权限的关联关系
        Integer roleId = role.getId();
        this.setRoleAndPermission(roleId,permissionIds);

    }

    //查询所有角色
    @Override
    public List<Role> findAll() {
        return roleDao.findAll();
    }

    //删除角色
    @Override
    public void deleteById(Integer id) {
        //判断当前角色是否已经关联到用户
        long count = roleDao.findCountByRoleId(id);
        if (count > 0){
            //大于零 代表已经关联到用户 无法删除
            new RuntimeException();
        }
        //根据id删除角色和权限的中间表
        roleDao.deleteAssocication(id);
        //根据id删除角色
        roleDao.deleteId(id);
    }
}
