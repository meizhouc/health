package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.MapDao;
import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.Map;
import com.itheima.service.MapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = MapService.class)
@Transactional
public class MapServiceImpl implements MapService {
    @Autowired
    private MapDao mapDao;

    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        PageHelper.startPage(currentPage,pageSize);
        Page<Map> page = mapDao.selectByCondition(queryString);
        long total = page.getTotal();
        List<Map> rows = page.getResult();
        return new PageResult(total,rows);
    }
    @Override
    public void add(Map map) {
        mapDao.add(map);
    }

    @Override
    public void deleteById(Integer id) {
        mapDao.deleteById(id);
    }

    @Override
    public List<Map> findAll() {
        return mapDao.findAll();
    }
}
