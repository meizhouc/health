package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.Permission;
import com.itheima.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service(interfaceClass = PermissionService.class)
@Transactional
public class PermissionServiceImpl implements PermissionService {

    //注入Dao对象
    @Autowired
    private PermissionDao permissionDao;

    //新增权限项
    @Override
    public void add(Permission permission) {
        permissionDao.add(permission);

    }

    //权限项分页查询
    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        //当前页码
        Integer currentPage = queryPageBean.getCurrentPage();
        //每页显示的记录数
        Integer pageSize = queryPageBean.getPageSize();
        //查询条件
        String queryString = queryPageBean.getQueryString();
        //完成分页查询 基于mybatis框架提供分页助手插件完成
        PageHelper.startPage(currentPage,pageSize);
        Page<Permission> page = permissionDao.selectByCondition(queryString);
        long total = page.getTotal();//总记录数
        List<Permission> rows = page.getResult();//当前页结果
        return new PageResult(total,rows);
    }

    //删除权限项
    @Override
    public void deleteById(Integer id) {
        //判断当前检查项是否已经关联到角色表
        long count = permissionDao.findCountByPermissionId(id);
        if (count > 0){
            //当前检查项已经被关联到检查组 不允许删除
            new RuntimeException();
        }
        permissionDao.deleteById(id);
    }

    //根据ID查询权限
    @Override
    public Permission findById(Integer id) {
        return permissionDao.findById(id);
    }

    //查询所有权限
    @Override
    public List<Permission> findAll() {
        return permissionDao.findAll();
    }

    //编辑权限项
    @Override
    public void edit(Permission permission) {
        permissionDao.edit(permission);
    }
}
