package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Role;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RoleDao {

    //新增角色
    public void add(Role role);

    //建立角色和权限的多对多关联关系
    public void setRoleAndPermission(Map<String, Integer> map);

    //分页查询
    public Page<Role> findByCondition(String queryString);

    //根据ID查询角色
    public Role findById(Integer id);

    //根据角色ID查询权限包含的多个角色ID
    public List<Integer> findPermissionIdsByRoleId(Integer id);

    //编辑角色
    public void edit(Role role);

    //清理当前角色关联的权限 操作中间表
    public void deleteAssocication(Integer id);

    //查询所有角色
    public List<Role> findAll();

    //根据用户id查询角色
    public Set<Role> findByUserId(Integer userId);

    //判断当前角色是否已经关联到用户
    public long findCountByRoleId(Integer id);

    //根据id 删除中间表t_user_role
    public void deleteRoleAndPermissionById(Integer id);

    //根据id 删除角色
    public void deleteId(Integer id);
}
