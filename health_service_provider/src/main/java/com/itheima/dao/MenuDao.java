package com.itheima.dao;

import com.itheima.pojo.Menu;

import java.util.List;

public interface MenuDao {

    //通过角色id查询可访问菜单
    List<Menu> findMenuByid(Integer id);
}
