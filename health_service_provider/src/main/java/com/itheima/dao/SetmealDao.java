package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealDao {

    //新增套餐信息
    public void add(Setmeal setmeal);

    public void setSetmealAndCheckItem(Map map);

    //分页查询
    Page<Setmeal> findByCondition(String queryString);

    //查询所有套餐
    List<Setmeal> findAll();

    //根据套餐ID查询套餐详细信息
    Setmeal findById(int id);
}
