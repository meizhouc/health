package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.OrderSetting;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface OrderSettingDao {
    public void add(OrderSetting orderSetting);
    //根据日期来修改
    public void editNumberByOrderDate(OrderSetting orderSetting);
    //根据日期来查询
    public long findCountByOrderDate(Date orderDate);

    //根据月份查询对应的预约设置数据
    List<OrderSetting> getOrderSettingByMonth(Map<String, String> map);

    OrderSetting findByOrderDate(Date orderDate);

    //更新已预约人数
    public void editReservationsByOrderDate(OrderSetting orderSetting);

    //按条件查询预约列表
    Page<Map<String, Object>> findOrdersPage(String queryString);

    //取消预约
    void delete(Integer id);

    //根据预约id查询用户手机
    String findphoneByid(Integer id);

    //根据预约id查询预约日期和用户电话
    Map<String, Object> confirm(Integer id);

    //通过预约id查询预约日期
    Date findDateByid(Integer id);
}
