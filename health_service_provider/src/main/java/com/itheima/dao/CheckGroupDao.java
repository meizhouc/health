package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

public interface CheckGroupDao {

    //新增检查组
    public void add(CheckGroup checkGroup);

    public void setCheckGroupAndCheckItem(Map map);

    //分页查询
    Page<CheckGroup> findByCondition(String queryString);

    //根据ID查询检查组
    CheckGroup findById(Integer id);

    //根据检查组ID查询检查组包含的多个检查项ID
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    void edit(CheckGroup checkGroup);

    void deleteAssocication(Integer id);

    //查询所有检查组
    List<CheckGroup> findAll();
}
