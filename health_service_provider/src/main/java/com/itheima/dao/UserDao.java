package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserDao {

    public User findByUsername(String username);

    //新增用户
    public void add(User user);

    //建立用户和角色的多对多关联关系
    public void setUserAndRole(Map<String, Integer> map);

    //分页查询
    public Page<User> findByCondition(String queryString);

    //根据ID查询角色
    public User findById(Integer id);

    //根据角色ID查询权限包含的多个角色ID
    public List<Integer> findRoleIdsByUserId(Integer id);

    //提交编辑用户表单
    public void edit(User user);

    public void deleteAssocication(Integer id);


    public void deleteId(Integer id);
}
