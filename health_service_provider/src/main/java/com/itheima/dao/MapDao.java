package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Map;

import java.util.List;

public interface MapDao {
    public Page<Map> selectByCondition(String queryString);
    public void add(Map map);
    public void deleteById(Integer id);
    public List<Map> findAll();
}
