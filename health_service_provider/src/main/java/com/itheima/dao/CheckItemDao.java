package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckItem;

import java.util.List;

public interface CheckItemDao {

    //添加检查项
    public void add(CheckItem checkItem);

    //分页查询
    public Page<CheckItem> selectByCondition(String queryString);

    //根据检查项ID统计数据量
    public long findCountByCheckItemId(Integer id);

    //删除检查项
    public void deleteById(Integer id);

    //编辑检查项
    public void edit(CheckItem checkItem);

    //根据ID查询数据
    CheckItem findById(Integer id);

    //查询检查项数据
    List<CheckItem> findAll();
}
