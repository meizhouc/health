package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Permission;

import java.util.List;
import java.util.Set;

public interface PermissionDao {

    //新增权限项
    public void add(Permission permission);

    //权限项分页查询
    Page<Permission> selectByCondition(String queryString);

    //根据权限项ID统计数据量
    public long findCountByPermissionId(Integer id);

    //删除权限项
    public void deleteById(Integer id);

    //根据ID查询数据
    public Permission findById(Integer id);

    //查询所有权限
    public List<Permission> findAll();

    //编辑权限项
    public void edit(Permission permission);

    //
    public Set<Permission> findByRoleId(Integer roleId);
}
