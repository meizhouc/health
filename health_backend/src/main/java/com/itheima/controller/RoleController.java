package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.entiy.Result;
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 角色管理
 */
@RestController
@RequestMapping("/role")
public class RoleController {
    @Reference
    private RoleService roleService;

    //新增角色
    @RequestMapping("/add")
    public Result add(@RequestBody Role role, Integer[] permissionIds){
        try {
            roleService.add(role,permissionIds);
        }catch (Exception e){
            e.printStackTrace();
            //新增失败
            return new Result(false, MessageConstant.ADD_ROLE_FAIL);
        }
        //新增成功
        return  new Result(true,MessageConstant.ADD_ROLE_SUCCESS);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return roleService.pageQuery(queryPageBean);
    }

    //根据ID查询角色
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            Role role = roleService.findById(id);
            return  new Result(true, MessageConstant.QUERY_ROLE_SUCCESS,role);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ROLE_FAIL);
        }
    }


    //根据角色ID查询权限包含的多个角色ID
    @RequestMapping("/findPermissionIdsByRoleId")
    public Result findPermissionIdsByRoleId(Integer id){
        try {
            List<Integer> permissionIds = roleService.findPermissionIdsByRoleId(id);
            return  new Result(true, MessageConstant.QUERY_PERMISSION_SUCCESS,permissionIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_PERMISSION_FAIL);
        }
    }

    //编辑角色
    @RequestMapping("/edit")
    public Result edit(@RequestBody Role role,Integer[] permissionIds){
        try {
            roleService.edit(role,permissionIds);
        }catch (Exception e){
            e.printStackTrace();
            //新增失败
            return new Result(false, MessageConstant.EDIT_ROLE_FAIL);
        }
        //新增成功
        return  new Result(true, MessageConstant.EDIT_ROLE_SUCCESS);
    }

    //查询所有角色
    @RequestMapping("/findAll")
    public Result findAll(){
        try {
            List<Role> list = roleService.findAll();
            //新增成功
            return  new Result(true, MessageConstant.QUERY_ROLE_SUCCESS,list);
        }catch (Exception e){
            e.printStackTrace();
            //新增失败
            return new Result(false, MessageConstant.QUERY_ROLE_FAIL);
        }
    }

    //删除角色
//    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")//权限校验
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try{
            roleService.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false,MessageConstant.DELETE_ROLE_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_ROLE_SUCCESS);
    }


}
