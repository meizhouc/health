package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.entiy.Result;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 用户操作
 */
@RestController
@RequestMapping("/user")
public class UserController {

    //获得当前登录用户的用户名
    @RequestMapping("/getUsername")
    public Result getUsername(){
        //当spring security完成认证后 会将当前用户信息保存到框架的上下文对象
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user != null){
            String username = user.getUsername();
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS,username);
        }
        return new Result(false,MessageConstant.GET_USERNAME_FAIL);
    }

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Reference
    private UserService userService;

    //新增用户
    @RequestMapping("/add")
    public Result add(@RequestBody com.itheima.pojo.User user,Integer[] roleIds) {

        String password = user.getPassword();
        user.setPassword(bCryptPasswordEncoder.encode(password));

        try {
            userService.add(user,roleIds);
        }catch (Exception e){
            e.printStackTrace();
            //新增失败
            return new Result(false, MessageConstant.ADD_USER_FAIL);
        }
        //新增成功
        return  new Result(true,MessageConstant.ADD_USER_SUCCESS);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return userService.pageQuery(queryPageBean);
    }

    //回显用户数据
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            com.itheima.pojo.User user = userService.findById(id);
            return  new Result(true, MessageConstant.QUERY_USER_SUCCESS,user);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_USER_FAIL);
        }
    }

    //根据角色ID查询权限包含的多个角色ID
    @RequestMapping("/findRoleIdsByUserId")
    public Result findRoleIdsByUserId(Integer id){
        try {
            List<Integer> roleIds = userService.findRoleIdsByUserId(id);
            return  new Result(true, MessageConstant.QUERY_ROLE_SUCCESS,roleIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ROLE_FAIL);
        }
    }

    //提交编辑表单
    @RequestMapping("/edit")
    public Result edit(@RequestBody com.itheima.pojo.User user, Integer[] roleIds){
        try {
            userService.edit(user,roleIds);
        }catch (Exception e){
            e.printStackTrace();
            //新增失败
            return new Result(false, MessageConstant.EDIT_USER_FAIL);
        }
        //新增成功
        return  new Result(true, MessageConstant.EDIT_USER_SUCCESS);
    }

    //删除用户
//    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")//权限校验
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try{
            userService.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false,MessageConstant.DELETE_USER_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_USER_SUCCESS);
    }

    //根据用户查询菜单
    @RequestMapping("/menu")
    public Result menu(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Result menu = null;
        try {
            menu = userService.menu(user.getUsername());
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,"查询菜单失败");
        }
        return menu;
    }
}
