package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.entiy.Result;
import com.itheima.pojo.Map;
import com.itheima.service.MapService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/mapinfo")
public class MapController {

    @Reference
    private MapService mapService;

    //地图分页
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return mapService.pageQuery(queryPageBean);
    }

    //查询所有分页
    @RequestMapping("/findAll")
    public Result findAll(){
        try {
            List<Map> list = mapService.findAll();
            return new Result(true, MessageConstant.QUERY_MAP_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_MAP_FAIL);
        }
    }

    //地图新增
    @RequestMapping("/add")
    public Result add(@RequestBody Map map){
        try {
            mapService.add(map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.ADD_MAP_FAIL);
        }
        return new Result(true,MessageConstant.ADD_MAP_SUCCESS);
    }

    //地图删除
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try {
            mapService.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.DELETE_MAP_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_MAP_SUCCESS);
    }

}
