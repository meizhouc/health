package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyuncs.exceptions.ClientException;
import com.itheima.constant.MessageConstant;
import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.entiy.Result;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.DateUtils;
import com.itheima.utils.POIUtils;
import com.itheima.utils.SMSUtils;
import org.apache.ibatis.annotations.Delete;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 预约设置
 */
@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {

    @Reference
    private OrderSettingService orderSettingService;

    //文件上传 实现预约设置数据批量导入
    @RequestMapping("/upload")
    public Result upload(@RequestParam("excelFile") MultipartFile excelFile) {
        try {
            //使用POI解析表格数据
            List<String[]> list = POIUtils.readExcel(excelFile);
            List<OrderSetting> data = new ArrayList<>();
            for (String[] strings : list) {
                String orderDate = strings[0];
                String number = strings[1];
                OrderSetting orderSetting = new OrderSetting(new Date(orderDate), Integer.parseInt(number));
                data.add(orderSetting);
            }
            //通过dubbo远程调用服务实现数据批量导入
            orderSettingService.add(data);
            return new Result(true, MessageConstant.IMPORT_ORDERSETTING_SUCCESS);

        } catch (IOException e) {
            e.printStackTrace();
            //文件解析失败
            return new Result(false, MessageConstant.IMPORT_ORDERSETTING_FAIL);
        }
    }

    //根据月份查询对应的预约设置数据
    @RequestMapping("/getOrderSettingByMonth")
    public Result getOrderSettingByMonth(String date) {
        try {
            List<Map> list = orderSettingService.getOrderSettingByMonth(date);
            return new Result(true, MessageConstant.GET_ORDERSETTING_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_ORDERSETTING_FAIL);
        }
    }

    //根据日期设置对应的预约设置数据
    @RequestMapping("/editNumberByDate")
    public Result editNumberByDate(@RequestBody OrderSetting orderSetting) {
        try {
            orderSettingService.editNumberByDate(orderSetting);
            return new Result(true, MessageConstant.ORDERSETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDERSETTING_FAIL);
        }
    }

    //分页查询预约列表
    @RequestMapping("/findOrdersPage")
    public PageResult findOrdersPage(@RequestBody QueryPageBean queryPageBean) {

        //封装返回数据
        PageResult pageResult = null;
        try {
            pageResult = orderSettingService.findOrdersPage(queryPageBean);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //返回数据
        return pageResult;

    }

    //新增预约
    @RequestMapping("/add")
    public Result add(Integer setmealid, @RequestBody Map map) {

        //封装预约类型
        map.put("orderType", Order.ORDERTYPE_TELEPHONE);
        try {
            Result result = orderSettingService.phoneAdd(setmealid, map);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDERSETTING_FAIL);
        }

    }

    //根据预约id确认预约
    @RequestMapping("/confirm")
    public Result confirm(Integer id) {

        try {
            //确认预约信息
            Map<String, Object> map = orderSettingService.confirm(id);

            if (map == null || map.size() == 0) {
                //预约信息不存在
                return new Result(false, MessageConstant.QUERY_ORDER_FAIL);
            }

            Date orderDate = (Date) map.get("orderDate");//预约日期
            String date = new SimpleDateFormat("yyyyMMdd").format(orderDate);//整理日期格式
            String phoneNumber = (String) map.get("phoneNumber");//用户号码
            //发送短信通知用户
            SMSUtils.sendShortMessage(SMSUtils.ORDER_NOTICE, phoneNumber, date);

            //返回确认成功
            return new Result(true, MessageConstant.ORDER_SUCCESS);

        } catch (ClientException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ORDER_FAIL);
        }

    }




    //批量确认预约
    @RequestMapping("/batchconfirm")
    public Result batchconfirm(@RequestBody List<Map> val){
        try {
            //遍历集合
            for (Map map : val) {
                //取出预约id
                confirm((Integer) map.get("id"));
            }
            return new Result(true, MessageConstant.ORDER_SUCCESS);//预约成功
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDERSETTING_FAIL);//预约失败
        }
    }

    //根据预约id取消预约
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            //取消预约
            String phone = orderSettingService.delete(id);

            //发送短信通知用户取消成功
            SMSUtils.sendShortMessage(SMSUtils.CENCEL_ORDER, phone, new SimpleDateFormat("yyyyMMdd").format(new Date()));

            return new Result(true, MessageConstant.DELETE_ORDER_SUCCESS);//取消成功
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_ORDER_FAIL);//取消失败
        }
    }

    //批量根据id取消预约
    @RequestMapping("/batchdelete")
    public Result batchdelete(@RequestBody List<Map> val) {
        try {
            //遍历集合
            for (Map map : val) {
                //取出预约id
                delete((Integer) map.get("id"));
            }
            return new Result(true, MessageConstant.DELETE_ORDER_SUCCESS);//取消成功
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_ORDER_FAIL);//取消失败
        }

    }
}
