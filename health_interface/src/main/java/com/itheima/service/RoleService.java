package com.itheima.service;

import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.Role;

import java.util.List;

public interface RoleService {
    //新增角色
    public void add(Role role, Integer[] permissionIds);

    //分页查询
    public PageResult pageQuery(QueryPageBean queryPageBean);

    //根据ID查询角色
    public Role findById(Integer id);

    //根据角色ID查询权限包含的多个角色ID
    public List<Integer> findPermissionIdsByRoleId(Integer id);

    //编辑角色
    public void edit(Role role, Integer[] permissionIds);

    //查询所有角色
    public List<Role> findAll();

    //删除角色
    public void deleteById(Integer id);
}
