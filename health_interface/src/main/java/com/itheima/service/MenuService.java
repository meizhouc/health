package com.itheima.service;

import com.itheima.pojo.Menu;

import java.util.List;

public interface MenuService {

    //根据用户查询菜单
    public List<Menu> findMenuByUserId(Integer id);

}
