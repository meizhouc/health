package com.itheima.service;

import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.entiy.Result;
import com.itheima.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

public interface OrderSettingService {

    public void  add(List<OrderSetting> data);
    //根据月份查询对应的预约设置数据
    List<Map> getOrderSettingByMonth(String date);

    //根据日期设置对应的预约设置数据
    void editNumberByDate(OrderSetting orderSetting);

    //预约列表分页查询
    PageResult findOrdersPage(QueryPageBean queryPageBean);

    //取消预约
    String delete(Integer id);

    //电话新增预约
    Result phoneAdd(Integer setmealid, Map map) throws Exception;

    //根据预约id查询预约日期和用户电话
    Map<String, Object> confirm(Integer id);

}
