package com.itheima.service;

import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.entiy.Result;
import com.itheima.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    public User findByUsername(String username);

    //新增用户
    public void add(User user, Integer[] roleIds);

    //分页
    public PageResult pageQuery(QueryPageBean queryPageBean);

    //根据ID查询用户信息
    public User findById(Integer id);

    //根据用户ID查询角色
    public List<Integer> findRoleIdsByUserId(Integer id);

    //提交编辑用户表单
    public void edit(User user, Integer[] roleIds);

    //删除用户
    public void deleteById(Integer id);

    //根据用户id查询用户可访问菜单
    Result menu(String username);


}
