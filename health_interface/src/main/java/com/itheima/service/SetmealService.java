package com.itheima.service;

import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealService {



    //新增套餐信息
    public void add(Setmeal setmeal ,Integer[]checkgroupIds);

    //分页查询
    PageResult pageQuery(QueryPageBean queryPageBean);

    //查询所有套餐
    List<Setmeal> findAll();

    //根据套餐ID查询套餐详细信息
    Setmeal findById(int id);

     List<Map<String, Object>> findSetmealCount();


}
