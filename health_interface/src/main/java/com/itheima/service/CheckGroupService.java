package com.itheima.service;

import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.CheckGroup;

import java.util.List;

public interface CheckGroupService {

    //新增检查组
    public void add(CheckGroup checkGroup,Integer[]checkitemIds);

    //分页查询
    PageResult pageQuery(QueryPageBean queryPageBean);

    //根据ID查询检查组
    CheckGroup findById(Integer id);

    //根据检查组ID查询检查组包含的多个检查项ID
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    //编辑检查组
    void edit(CheckGroup checkGroup, Integer[] checkitemIds);

    //查询所有检查组
    List<CheckGroup> findAll();
}
