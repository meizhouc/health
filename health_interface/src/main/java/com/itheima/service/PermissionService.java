package com.itheima.service;

import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.Permission;

import java.util.List;

public interface PermissionService {
    //新增权限项
    public void add(Permission permission);

    //权限项分页查询
    public PageResult pageQuery(QueryPageBean queryPageBean);

    //删除权限项
    public void deleteById(Integer id);

    //根据ID查询权限
    public Permission findById(Integer id);

    //查询所有权限
    public List<Permission> findAll();

    //编辑权限
    public void edit(Permission permission);
}
