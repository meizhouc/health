package com.itheima.service;

import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.CheckItem;

import java.util.List;

//服务接口
public interface CheckItemService {

    //新增检查项
    public  void add(CheckItem checkItem);

    //检查项分页查询
    public PageResult pageQuery(QueryPageBean queryPageBean);

    //删除检查项
    void deleteById(Integer id);

    //编辑检查项
    void edit(CheckItem checkItem);

    //根据ID查询数据
    CheckItem findById(Integer id);

    //查询检查项数据
    List<CheckItem> findAll();
}
