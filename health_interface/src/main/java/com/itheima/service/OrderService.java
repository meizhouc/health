package com.itheima.service;

import com.itheima.entiy.Result;

import java.util.List;
import java.util.Map;

public interface OrderService {
    Result order(Map map) throws Exception;
    Map findById(Integer id) throws Exception;
    List<String> findMap();
}
