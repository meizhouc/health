package com.itheima.service;

import com.itheima.entiy.PageResult;
import com.itheima.entiy.QueryPageBean;
import com.itheima.pojo.Map;

import java.util.List;

public interface MapService {
    public PageResult pageQuery(QueryPageBean queryPageBean);
    public void add(Map map);
    public void deleteById(Integer id);
    public List<Map> findAll();
}
